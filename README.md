## OPDRACHT

- Maak een Strava en Strava API account aan [api account](https://developers.strava.com/docs/)
- Maak een tooltje waarmee je

1. De 10 populairste [segmenten](https://developers.strava.com/docs/reference/#api-Segments-exploreSegments) in de steden Utrecht, Amsterdam en Eindhoven kan opvragen. Dit moet via een multi select filter werken. Ik kan steden dus desgewenst aan- of uitvinken. Selectie gaat op basis van een rechthoekige bounding box, dus we snappen dat je niet precies de stadsgrenzen kunt volgen. Van deze segmenten willen we in ieder geval de Naam, stad en lengte zien.
2. Voeg daarbij een filter toe waarin je de maximum en minimum afstand van een segment aan kan geven. Filtering moet real time gebeuren, maar denk aan performance en je API rate.
3. Zorg ervoor dat je laatst actieve filters preselected zijn als je de website nog eens bezoekt.

## (Technische) randvoorwaarden

1. Gebruik een FE framework naar keuze (Vue / angular / react)
2. Gebruik een component library en of css framework naar keuze. Beperk dus het maatwerk ;)
3. Het hoeft er niet flashy uit te zien, maar moet wel responsive zijn opgezet
4. Submit je werk in een feature branch met je eigen naam. Zorg dat je werk op de starten is met een `npm start` commando.
